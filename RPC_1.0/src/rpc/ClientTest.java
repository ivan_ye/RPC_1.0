package rpc;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Client端测试类
 * 
 * @author 宋挺
 */
public class ClientTest {
	public static void main(String[] args) throws UnknownHostException, IOException {
		// 获取一个 Client端的代理对象 proxy
		Login proxy = (Login) RPC.getProxy(Login.class, "192.168.8.1", 8888);
		// 调用 proxy 的 login() 方法, 返回值为 res
		String res = proxy.login("rpc", "password");
		// 输出 res
		System.out.println(res);
	}
}
