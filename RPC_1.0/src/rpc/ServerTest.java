package rpc;

import java.io.IOException;

/**
 * Server端测试类
 * 
 * @author 宋挺
 */
public class ServerTest {
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		// 获取 RPC 的 Server 端实例 server
		RPCServer server = RPC.getRPCServer(Login.class, new LoginImpl(), 8888);
		// 循环监听并接收 Client 端连接, 处理 RPC 请求, 向 Client 端返回结果
		server.start();
	}
}
